## WvW zerg builds

This only includes builds which I specifically have experiences with. Ratings:

* Mandatory: 5/5
* Awesome: 4/5
* Great: 3/5
* Good: 2/5
* OK: 1/5

[[_TOC_]]

### Guardian

#### (Mandatory) [Minstrel support Firebrand](http://gw2skills.net/editor/?PWwAk6ZlRwEZYMOmJm2WrvbA-zVJYjRH/ZkqUIdVgGPEhZP8AA-w)

* Recommendation: **Mandatory**
* Strengths:
  * Awesome stability
  * Great protection
  * Great aegis
  * Great resistence
  * Great swiftness
  * Great projectile hate
  * Good group stun break
  * Good crowd control
  * Good condition cleanes
  * Good healing
  * Good group endurance regeneration buff
* Weakness:
  * Cannot provide enough condition cleanses and healing for the party alone
* In game link: `[&DQExGy4rPjdLFwAAFgEAADYBAABLAQAAGBYAAAAAAAAAAAAAAAAAAAAAAAA=]`

#### (Good) [Assassin damage Dragonhunter](http://gw2skills.net/editor/?PWwAMt/lFwQYOsG2IO0X1NVA-zVRYbRNnvYQBDSzQoeCUpCMdACYP8AA-w)

* Recommendation: **Good**
* Strengths:
  * Great range burst
  * Great melee burst
  * Good crowd control
  * Good survival
  * Can bring stability
  * Can swap to support Firebrand
* Weakness:
  * OK melee cleave
  * Susceptible to burst (Low health pool and slower stun break)
* In game link: `[&DQEqOi41GyoDAQAASAEAAIoSAAD+EgAANwEAAAAAAAAAAAAAAAAAAAAAAAA=]`

### Revenant

#### (Great) [Berserker damage Herald](http://gw2skills.net/editor/?PmwAIxzlpQHMLyhdRNMM6hJSfsCKgJ/la7G-zVRYBRN3tYQIDSzQoeCUpCMdACYP8AA-w)

* Recommendation: **Great**
* Strengths:
  * Great range burst
  * Great melee burst
  * Good melee cleave
  * Great fury
  * Great swiftness
  * Good might
  * Good stability
  * Can swap to healing Vindicator
* Weakness:
  * Very susceptible to conditions
  * Susceptible to crowd control (Stun break under Jalis takes too much energy to use under emergency)
* In game link: `[&DQkDPg8qNDfcEQAABhIAACsSAADUEQAAyhEAAAEDAAArEgYS1BEAAAAAAAA=]`

#### (Good) [Berserker damage Vindicator](http://gw2skills.net/editor/?PmgAEJlZSHsTyhhSNsU6hpSfsSKgJ/lasD-zVRYBRF3tYQIFSTRQmpQFFgJHgA2DPAA-w)

* Recommendation: **Good**
* Strengths:
  * Great range burst
  * Great melee burst
  * Awesome melee cleave
  * Good stability
  * Good vigor
  * Good survival
  * Can swap to healing Vindicator
* Weakness:
  * Can only provide good values when go melee
* In game link: `[&DQkDPg8qRR3cEQAABhIAACsSAADUEQAAyhEAAAcDAAArEgYS1BEAAAAAAAA=]`

#### (Great) [Minstrel healing Vindicator](http://gw2skills.net/editor/?PmgAYZlZSJsTiihSTsUqipShsSagl0zf8H-zVJYjRFfZkZKUdF47hQ0YYPMAA-w)

* Recommendation: **Great**
* Strengths:
  * Great condition cleanse
  * Great healing
  * Great vigor
  * Good regeneration
  * Great group stun break
  * Great survival
  * Good projectile hate
  * Can bring stability
* Weakness:
  * Cannot provide a wide range of boons
* In game link: `[&DQkJKQw/RT/cEQAABhIAACsSAADUEQAAyhEAAAcGAAArEgYS1BEAAAAAAAA=]`

### Warrior

#### (OK) [Berserker damage Berserker](http://gw2skills.net/editor/?PKgAMFlRwmYgMKGJO+W6tKA-zVIYRUxPINTg6hQlKw0BIg9wDA-w)

* Recommendation: **OK**
* Strengths:
  * Awesome melee burst via Arc Divider
  * Great range sustain damage via Scorched Earth and Arcing Arrow
  * Great revive via Battle Standard
  * Great health pool
  * Can rush to catch up or escape
* Weakness:
  * Can hardly deal any ranged damage without going Berserk, therefore long downtime when Berserk is on cooldown
  * OK range burst
  * OK melee cleave
* In game link: `[&DQIEHzM6EhXyEgAAqQAAAKsAAACvAAAA7gAAAAAAAAAAAAAAAAAAAAAAAAA=]`

### Necromancer

#### (Great) [Berserker+Assassin boon strip Scourge](http://gw2skills.net/editor/?PSABsid3lNwOYIsHmJOqL6vcA-z1QYhontDeQamA1DhKVgpDQA7hHA-w)

* Recommendation: **Great**
* Strengths:
  * Great midrange boon corruption
  * Great melee boon corruption
  * Great midrange burst
  * Great midrange cleave
  * Good melee burst
  * Good stability
  * Great cover conditions
* Weakness:
  * Susceptible to falling behind (Slow to catch up)
* In game link: `[&DQg1NSc6PDl+FgAAgQAAAHMBAAByFgAAYBcAAAAAAAAAAAAAAAAAAAAAAAA=]`

### Mesmer

#### (Great) [Berserker+Assassin damage Virtuoso](http://gw2skills.net/editor/?PiwAEh3llyWZWMVmLOOLZRLA-z1QYhom2DaHio3BpZIUPBqUBmOABsHeAA-w)

* Recommendation: **Great**
* Strengths:
  * Awesome range burst
  * Great range cleave
  * Great survival
  * Great stability
  * Good boon strip
* Weakness:
  * Psychic Riposte is reliant on enemy attacks
  * Susceptible to conditions
  * OK melee cleave
* In game link: `[&DQcKJwEZQhbbGgAA3RoAAIUBAADcGgAA5RoAAAAAAAAAAAAAAAAAAAAAAAA=]`

#### (Good) [Celestial boon strip Chronomancer](http://gw2skills.net/editor/?PiwAw63lVwcYUMLWJOuWttWA-z1IY1ol/M6WBkzUQjHiwsFeAA-w)

* Recommendation: **Good**
* Strengths:
  * Great midrange crowd control
  * Good midrange boon strip
  * Great melee boon strip
  * Great stability
  * Good quickness
  * Good melee cover conditions
  * Great survival
  * Can bring good utilities
* Weakness:
  * Can hardly strip boons if the enemies have good stability coverage
* In game link: `[&DQcKFxgtKC0jDwAAhxIAAGcBAACFAQAAsxIAAAAAAAAAAAAAAAAAAAAAAAA=]`

### Elementalist

#### (Great) [Minstrel healing Tempest](http://gw2skills.net/editor/?PGwAgitlRwaYMMHWJOWTptfA-zVJYjRHfhkUEkaBUdC47hIA7gHA-w)

* Recommendation: **Great**
* Strengths:
  * Great condition cleanse
  * Great healing
  * Great vigor
  * Great regeneartion
  * Great protection and damage reduction (Protection buff and Frost aura)
  * Great chill (Frost aura)
  * Good group stun break
  * Can bring good might
  * Can bring good projectile hate
* Weakness:
  * Cannot provide a wide range of boons
* In game link: `[&DQYfKxEpMD/HEgAAdBIAAHYSAAClEgAAmRIAAAAAAAAAAAAAAAAAAAAAAAA=]`

#### (Great) [Minstrel support Tempest](http://gw2skills.net/editor/?PGwAg2hlRwaYMsFWJOWTrtXA-zVJYjRH/hkmIIVFQ6SANeICzO4BA-w)

* Recommendation: **Great**
* Strengths:
  * Great condition cleanse
  * Great vigor
  * Great regeneartion
  * Great protection
  * Great alacrity
  * Awesome might
  * Good group stun break
  * Good crowd control
  * Good projectile hate
* Weakness:
  * Cannot provide enough sustain healing
* In game link: `[&DQYfKxErMC/HEgAAdBIAAHYSAADDEgAAmRIAAAAAAAAAAAAAAAAAAAAAAAA=]`

### Experimental builds

This includes builds which I haven't tried or don't have enough experiences with.

* [Berserker+Assassin damage Scourge](http://gw2skills.net/editor/?PSwAc2jlNwOYIsHmJOSXlvcA-z1QYholtDeQaGC1TgKFgpDQAbhHA-w)
