## PvE builds

This only includes builds which I specifically have experiences with. Ratings:

[[_TOC_]]

### Guardian

#### Celestial Quickness Firebrand

* :white_check_mark: [Solo/support Firebrand](http://gw2skills.net/editor/?PWwAc+tlRweYdMIGJW0WfPTA-zxIY1ojvMSoC6IBk/QIo0fc6gDwz0jAA-e)
* :black_square_button: [Heal variant](http://gw2skills.net/editor/?PWwAc+tlRweYdMRGJW0WrPTA-zxIY1ojvMSoC6IBk/QIo0fc6gDwz0jAA-e) (minor trait difference)
* :white_check_mark: [Heal+ variant](http://gw2skills.net/editor/?PWwAc+tlRweYJMRGJW0SrPbA-zxIY1ojvMSoC6IBk/QIo0fc6gDwz0jA-e) (Honor)
* :white_check_mark: [Heal++ variant](http://gw2skills.net/editor/?PWwAc+ZlRweYJMR2JmOXrPbA-zxIY1ojvMSNFquCk/QIo0fc6hDwz0jAA-e) (No Radiance, Staff)
* :black_square_button: [Heal+++ variant](http://gw2skills.net/editor/?PWwAk6ZlRweYJMR2JmKXrPbA-zxIY1ojvMSNFquC89QEg9wnpHBA-e) (Shield, Staff, Monk)
* :white_check_mark: [Seraph variant](http://gw2skills.net/editor/?PWwAc+tlRweYdMIGJW0WfPTA-zRRYoRHXtcQKhRSF0lAOHgw1BfmeEA-e) (Full DPS variant)

### Engineer

#### Celestial Alacrity Mechanist

* :white_check_mark: [Solo/support Mechanist](http://gw2skills.net/editor/?PegAkqlxygZusWWMO2L7RVA-zxIY1ohvMioA6IBk/AEuG4z0jAA-e)
* :white_check_mark: [Stacked DPS variant](http://gw2skills.net/editor/?PegAkqlxygZusXWMO2L7xcA-zxIY1ohvMioA6IB08AEuG4z0jAA-e)
* :black_square_button: [Stacked vulnerability+ variant](http://gw2skills.net/editor/?PegAkqlxygZusXWMOuL7xdA-zxIY1ohvMioA6IB08AEuG4z0jAA-e)

### Mesmer

#### Celestial Alacrity Mirage

* :white_check_mark: [Solo/support Mirage](http://en.gw2skills.net/editor/?PigAYZlRwiYJMEmJWKPdPNA-zxIY1oivMyMFCpCk/AEue4z0jA-e)
* :black_square_button: [More defence variant](http://gw2skills.net/editor/?PigAYZlRwiYJMEmJWyP9PNA-zxIY1oivMyMFCpCk/AEue4z0jA-e)
* :black_square_button: [Group boss variant](http://gw2skills.net/editor/?PigAYZlRwiYJMEmJWKPdvKA-zxIY1oivMyMFCpCk/AEue4z0jA-e)

#### DPS Chronomancer

* :white_check_mark: [Power Chronomancer](http://gw2skills.net/editor/?PiwAExzlVwYYbsJWJOyKdNfA-zxQYhoGFsDnfZUbKURFYyBIg9wnpHBA-e) (Assassin + Berserker)

#### DPS Virtuoso

* :white_check_mark: [Condition Virtuoso](http://gw2skills.net/editor/?PiwAgy3lVwWZJMEmLW6WdxdA-zRRYmhDsIaRfjRhUIeB47AEuW4z0jAA-e) (Viper + Rampager)
* :white_check_mark: [Power Virtusos](http://gw2skills.net/editor/?PiwAgyFlVwWZbsJmLOyKdRdA-zRIYR0xXG1mCVUBmcACYO8Z6RAA-e)

### Revenant

#### Celestial Quickness Herald

* :white_check_mark: [Solo/support Herald](http://gw2skills.net/editor/?PmwAwyZlpQMMLyjdRdMMijJSksCigJ7larE-zxIY1oj/MiQKUhKwjHAB2Dk+M9IA-e)
* :black_square_button: [Heal variant](http://gw2skills.net/editor/?PmwAwyZlpQJMLiidRTMMqiJShsCagnqkdrE-zxIY1oj/MiQKUhKwjHAB2Dk+M9IA-e) (Salvation)

### Necromancer

#### Celestial Scourge

* :white_check_mark: [Solo Scourge](http://en.gw2skills.net/editor/?PSwEs8NsMCGI7gpxIxi9q+ZC-zxIY1olvM6oACJFE+AEuW4z0jAA-e)

#### Celestial Harbinger

* :black_square_button: [Solo Harbinger](http://en.gw2skills.net/editor/?PSAFoE1aYZoMQGLLlRiF7Vd8H-zxIY1onvM6qC6IFE+AEue4z0jAA-e)
